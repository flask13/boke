from flask_migrate import MigrateCommand

from apps import create_app
from apps.models.photo import Photo
from apps.models.user import User,Personal_information
from apps.models.ariticle import Article_type,Article,Comment,Like,Favorite,Message
from exts import db, manager

app = create_app()
manager.add_command('db',MigrateCommand)




if __name__ == '__main__':
    manager.run()