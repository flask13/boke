from flask_uploads import configure_uploads,patch_request_class
from flask import Flask,render_template
from flask_uploads import configure_uploads,patch_request_class
from apps.views.article import article_bp
from apps.views.photo import photo_bp

from apps.views.user import user_bp
from config import Baseconfig
from exts import db, migrate, bootstrap, mail, cache, loginmanager, photos

#工厂函数
from exts.test import test_bp


def create_app():
    app = Flask(__name__,template_folder='../templates',static_folder='../static')
    app.config.from_object(Baseconfig)#基本配置文件
    db.init_app(app)
    migrate.init_app(app,db)#数据库迁移
    register_blueprint(app)#蓝图注册
    bootstrap.init_app(app)
    config_errorhandler(app)#错误信息配置函数
    mail.init_app(app)#发邮件初始化
    cache.init_app(app,config=Baseconfig.config)#redis初始化
    config_login(app)#配置login
    config_upload(app)
    return app


#蓝图注册
def register_blueprint(app):
    app.register_blueprint(user_bp)
    app.register_blueprint(article_bp)
    app.register_blueprint(test_bp)
    app.register_blueprint(photo_bp)



#错误页面显示
def config_errorhandler(app):
    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('error/404.html')

#配置login
def config_login(app):
    loginmanager.init_app(app)
    #会话保护机制
    loginmanager.session_protection = 'strong'
    #设置视图端点，当访一个页面访问需要登录的时候，如果不在登录状态，则自动跳转到这个页面
    loginmanager.login_view = 'user_bp.login'
    #设置提示信息
    loginmanager.login_message = '页面需要登录才能访问'


#上传文件配置
def config_upload(app):
    configure_uploads(app,photos)
    patch_request_class(app,size=6*1024*1024)#容许最大文件大小
