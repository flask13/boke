from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap
from flask_mail import Mail,Message
from flask_caching import Cache
from flask_login import LoginManager
from flask_uploads import UploadSet,IMAGES

db = SQLAlchemy()#定义数据库
manager = Manager()#管理app
migrate = Migrate()#创建迁移第三方库
bootstrap = Bootstrap()
mail = Mail()#创建发邮件对象
cache = Cache()
loginmanager = LoginManager()
photos = UploadSet('photos',IMAGES)#创建对象，一个名字，一个扩展名



