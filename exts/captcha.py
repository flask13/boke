import random

from PIL import Image,ImageDraw,ImageFont


#随机生成一个颜色
from flask import flash

from exts import cache


def generate_color():
    return (random.randint(0,255),random.randint(0,255),random.randint(0,255))

#随机生成图形验证码
def generate_captcha(length):
    size = (80,35)#定义尺寸
    im = Image.new('RGB',size,color=generate_color())#定义画布
    font = ImageFont.truetype(font='font/FZSTK.TTF',size=30)#定义字体
    draw = ImageDraw.Draw(im)#创建一个画的动作
    s = 'hjdskjfidsjf9wuewjdfiwu89e9re903u93jr3urj8930rcj90e3cc3'
    code = ''
    for i in range(length):
        data = random.choice(s)
        code += data
        draw.text((5+16*i+random.randint(0,5),random.randint(0,10)-5),text=data,fill=generate_color(),font=font)
    for i in range(5):
        x1 = random.randint(0,100)
        y1 = random.randint(0,20)
        x2 = random.randint(0,100)
        y2 = random.randint(20,40)
        draw.line(((x1,y1),(x2,y2)))
    return im,code




