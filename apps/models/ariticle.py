from sqlalchemy import and_

from exts import db
from datetime import datetime
from flask_login import current_user

class Article_type(db.Model):
    id = db.Column(db.INTEGER,primary_key=True,autoincrement=True)
    type = db.Column(db.String(20),nullable=False)
    article = db.relationship('Article',backref='article_type',lazy='dynamic')



#文章模型
class Article(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    title = db.Column(db.String(50),nullable=False)
    content = db.Column(db.Text,nullable=False)
    datetime = db.Column(db.DateTime,default=datetime.now)
    Favorites = db.Column(db.INTEGER,default=0)
    like = db.Column(db.INTEGER,default=0)
    type_id = db.Column(db.ForeignKey('article_type.id'))
    user_id = db.Column(db.ForeignKey('user.id'))
    comment = db.relationship('Comment',backref='article',lazy='dynamic')
    likes = db.relationship('Like', backref='article', lazy='dynamic')
    favorite = db.relationship('Favorite', backref='article', lazy='dynamic')




#文章评论表
class Comment(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    content = db.Column(db.Text, nullable=False)
    datetime = db.Column(db.DateTime, default=datetime.now)
    article_id = db.Column(db.ForeignKey('article.id'))
    user_id = db.Column(db.ForeignKey('user.id'))

#点赞表（用户和文章关联表）
class Like(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    user_id = db.Column(db.ForeignKey('user.id'))
    article_id = db.Column(db.ForeignKey('article.id'))
    datetime = db.Column(db.DateTime, default=datetime.now)


    #判断是否已经点赞
    @staticmethod
    def is_like(aid):
        if Like.query.filter(and_(Like.user_id==current_user.id,Like.article_id==aid)).first():
            return True
        else:
            return False


#收藏表（用户和文章关联表）
class Favorite(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    user_id = db.Column(db.ForeignKey('user.id'))
    article_id = db.Column(db.ForeignKey('article.id'))
    datetime = db.Column(db.DateTime, default=datetime.now)


    #判断是否已经点赞
    @staticmethod
    def is_favorite(aid):
        if Favorite.query.filter(and_(Favorite.user_id==current_user.id,Favorite.article_id==aid)).first():
            return True
        else:
            return False


#留言
class Message(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    username = db.Column(db.String(50),nullable=False)
    phone = db.Column(db.String(11),nullable=False)
    email = db.Column(db.String(50))
    content = db.Column(db.Text,nullable=False)
    datetime = db.Column(db.DateTime,default=datetime.now)
