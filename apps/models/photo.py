from exts import db
from datetime import datetime


class Photo(db.Model):
    id = db.Column(db.INTEGER,primary_key=True,autoincrement=True)
    photo_name = db.Column(db.String(50),nullable=False)
    p_datetime = db.Column(db.DateTime,default=datetime.now)
    user_id = db.Column(db.ForeignKey('user.id'))
