from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask_login import UserMixin
from sqlalchemy.orm import backref

from config import Baseconfig
from exts import db, loginmanager
from datetime import datetime


class User(UserMixin,db.Model):
    id = db.Column(db.INTEGER,primary_key=True,autoincrement=True)
    username = db.Column(db.String(20),nullable=False)
    password = db.Column(db.String(128),nullable=False)
    email = db.Column(db.String(50),nullable=False)
    phone = db.Column(db.String(11),nullable=False)
    icon = db.Column(db.String(50),default='static/upload/default.jpg')
    activation = db.Column(db.Boolean,default=0)
    state = db.Column(db.Boolean,default=1)
    datetime = db.Column(db.DateTime,default=datetime.now)
    photo = db.relationship('Photo', backref='user',lazy='dynamic')
    article = db.relationship('Article', backref='user', lazy='dynamic')
    comment = db.relationship('Comment', backref='user', lazy='dynamic')
    likes = db.relationship('Like', backref='user', lazy='dynamic')
    favorite = db.relationship('Favorite', backref='user', lazy='dynamic')


    def __str__(self):
        return self.username


    #生成token
    def genereate_token(self):
        s = Serializer(Baseconfig.SECRET_KEY,expires_in=3600)
        token = s.dumps({'id':self.id})#把字典转化为字符串
        return token

    #激活token
    @staticmethod
    def active_token(token):
        s = Serializer(Baseconfig.SECRET_KEY)
        try:
            data = s.loads(token)#把字符串转化为字典
        except:
            return False
        #更改激活状态为True
        user = User.query.get(data.get('id'))
        user.activation = True
        db.session.commit()
        return True

#回调函数
@loginmanager.user_loader
def loader_user(user_id):
    return User.query.get(int(user_id))



#博主个人信息表
class Personal_information(db.Model):
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    content = db.Column(db.Text,nullable=False)
