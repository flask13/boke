from flask_mail import Message
from flask import render_template,current_app


from config import Baseconfig
from exts import mail
from threading import Thread

#邮件发送
def send_mail(subject,recipients,html,**kwargs):
    app = current_app._get_current_object()#得到原生的app
    msg = Message(subject=subject,recipients=recipients,html=html,sender=Baseconfig.MAIL_USERNAME)#定义消息
    msg.html = render_template(html,**kwargs)#邮件正文
    thr = Thread(target=async_send_mail,args=[app,msg])#创建线程
    thr.start()#线程启动

#异步发送
def async_send_mail(app,msg):
    with app.app_context():#程序上下文
        mail.send(msg)