from flask import Blueprint, flash, request, redirect, render_template, url_for,jsonify
from flask_login import login_required,current_user
from sqlalchemy import and_, or_

from apps.models.ariticle import Article_type, Article, Comment, Like, Favorite
from exts import db
from exts.like import del_like, get_like, get_favorite, del_favorite

article_bp = Blueprint('article_bp',__name__,url_prefix='/article')#创建蓝图

#添加文章类型
@article_bp.route('/add_type',methods=['GET','POST'])
def add_type():
    types = Article_type.query.all()
    if request.method == 'POST':
        type = request.form.get('type')
        article_type = Article_type()
        article_type.type = type
        db.session.add(article_type)
        db.session.commit()
        flash('文章类型添加成功')
        return redirect(url_for('article_bp.show_type'))
    return render_template('article/add_type.html',types=types)


#修改文章类型
@article_bp.route('/modify_type',methods=['GET','POST'])
def modify_type():
    types = Article_type.query.all()
    if request.method == 'POST':
        tid = request.form.get('tid')
        type = request.form.get('type')
        article_type = Article_type.query.get(tid)
        article_type.type = type
        db.session.commit()
        flash('类型修改成功')
        return redirect(url_for('article_bp.show_type'))
    tid = request.args.get('tid')
    article_type = Article_type.query.get(tid)
    return render_template('article/modify_type.html',article_type=article_type,types=types)



#删除文章类型
@article_bp.route('/del_type',methods=['GET','POST'])
def del_type():
    tid = request.args.get('tid')
    article_type = Article_type.query.get(tid)
    db.session.delete(article_type)
    db.session.commit()
    flash('类型删除成功')
    return redirect(url_for('article_bp.show_type'))


#显示文章类型
@article_bp.route('show_type',methods=['GET','POST'])
def show_type():
    types= Article_type.query.all()
    return render_template('article/show_type.html',types=types)


#文章发布
@article_bp.route('/publish',methods=['GET','POST'])
@login_required
def publish():
    if request.method == 'POST':
        title = request.form.get('title')
        content = request.form.get('content')
        type_id = request.form.get('type_id')
        article = Article()
        article.title = title
        article.content = content
        article.type_id = type_id
        article.user_id = current_user.id
        db.session.add(article)
        db.session.commit()
        flash('文章发布成功')
        return redirect(url_for('user_bp.index'))
    types = Article_type.query.all()
    return render_template('article/publish_article.html',types=types)


#文章详情页
@article_bp.route('/details',methods=['GET','POST'])
def details():
    types = Article_type.query.all()
    aid = request.args.get('aid')
    article = Article.query.filter(Article.id==aid).first()
    page = request.args.get('page', 1, type=int)
    pagination = Comment.query.filter(Comment.article_id == aid).order_by(Comment.datetime.desc()).paginate(page=page, per_page=5, error_out=False)
    return render_template('article/details.html',article=article,pagination=pagination,types=types)



#发布评论
@article_bp.route('/comment',methods=['GET','POST'])
@login_required
def comment():
    types = Article_type.query.all()
    aid = request.form.get('aid')
    article = Article.query.filter(Article.id == aid).first()
    c_content = request.form.get('c_content')
    comment = Comment()
    comment.content = c_content
    comment.article_id = aid
    comment.user_id = current_user.id
    db.session.add(comment)
    db.session.commit()
    page = request.args.get('page',1,type=int)
    pagination = Comment.query.filter(Comment.article_id == aid).order_by(Comment.datetime.desc()).paginate(page=page,per_page=5,error_out=False)
    flash('评论成功')
    return render_template('article/details.html',article=article,pagination=pagination,types=types)



#点赞实现
@article_bp.route('/like',methods=['GET','POST'])
@login_required
def like():
    aid = request.args.get('aid')
    article = Article.query.filter(Article.id == aid).first()
    if Like.is_like(aid):#判断是否已经点赞
        del_like(aid)#取消点赞
    else:
        get_like(aid)#增加点赞
    db.session.commit()
    return jsonify(num=article.like)



#收藏实现
@article_bp.route('/favorite',methods=['GET','POST'])
@login_required
def favorite():
    aid = request.args.get('aid')
    article = Article.query.filter(Article.id == aid).first()
    if Favorite.is_favorite(aid):#判断是否已经收藏
        del_favorite(aid)#取消收藏
    else:
        get_favorite(aid)#增加收藏
    db.session.commit()
    return jsonify(num=article.Favorites)


#显示评论
@article_bp.route('/show_comment',methods=['GET','POST'])
@login_required
def show_comment():
    types = Article_type.query.all()
    page = request.args.get('page',1,type=int)
    pagination = Comment.query.filter(Comment.user_id==current_user.id)\
        .order_by(Comment.datetime.desc()).paginate(page=page,per_page=5,error_out=False)
    return render_template('comment/show_comment.html',pagination=pagination,types=types)


#删除评论
@article_bp.route('/del_comment/',methods=['GET','POST'])
@login_required
def del_comment_11():
    cid = request.args.get('cid')
    comment = Comment.query.filter(Comment.id==cid).first()
    if comment:
        db.session.delete(comment)
        db.session.commit()
        flash('评论删除成功')
    else:
        flash('该评论不存在')
    return redirect(url_for('article_bp.show_comment'))


#文章分类显示
@article_bp.route('/article_class_show/',methods=['GET','POST'])
@login_required
def article_class_show():
    types = Article_type.query.all()
    tid = request.args.get('tid')
    page = request.args.get('page',1,type=int)
    pagination = Article.query.filter(Article.type_id==tid).paginate(page=page,per_page=5,error_out=False)
    return render_template('article/article.html',pagination=pagination,types=types,tid=tid)


#显示当前用户的文章
@article_bp.route('/show_user_article/',methods=['GET','POST'])
@login_required
def show_user_article():
    types = Article_type.query.all()
    page = request.args.get('page', 1, type=int)
    pagination = Article.query.filter(Article.user_id==current_user.id).paginate(page=page,per_page=5,error_out=False)
    return render_template('article/show_user_article.html',pagination=pagination,types=types)



#修改文章
@article_bp.route('/modify_article/',methods=['GET','POST'])
@login_required
def modify_article():
    if request.method == 'POST':
        aid = request.form.get('aid')
        title = request.form.get('title')
        content = request.form.get('content')
        article = Article.query.filter(Article.id==aid).first()
        article.title=title
        article.content=content
        db.session.commit()
        flash('文章修改成功')
        return redirect(url_for('article_bp.show_user_article'))
    types = Article_type.query.all()
    aid = request.args.get('aid')
    article = Article.query.filter(Article.id==aid).first()
    return render_template('article/modify_article.html',article=article,types=types)

#删除文章
@article_bp.route('/del_article/',methods=['GET','POST'])
@login_required
def del_article():
    aid = request.args.get('aid')
    article = Article.query.filter(Article.id == aid).first()
    db.session.delete(article)
    db.session.commit()
    flash('文章删除成功')
    return redirect(url_for('article_bp.show_user_article'))


#通过关键词搜索文章
@article_bp.route('/search/',methods=['GET','POST'])
def search():
    types = Article_type.query.all()
    keyword = request.form.get('search')
    page = request.args.get('page',1,type=int)
    pagination = Article.query.filter(Article.title.contains(keyword)).paginate(page=page,per_page=5,error_out=False)
    if not pagination.items:
        flash('没有您要找的相关文章')
    return render_template('article/search_article.html', pagination=pagination, types=types)