import os

from flask import Blueprint, request, render_template, flash, redirect, url_for
from flask_login import login_required,current_user

from apps.models.ariticle import Article_type
from apps.models.photo import Photo
from config import Baseconfig
from exts import db
from exts.ali_yun import file_upload, file_del

photo_bp = Blueprint('photo_bp',__name__,url_prefix='/photo')


#图片上传
@photo_bp.route('/photo_upload',methods=['GET','POST'])
@login_required
def photo_upload():
    types = Article_type.query.all()
    if request.method == 'POST':
        photo = request.files.get('photo')
        ret,filename = file_upload(photo)
        if ret.status == 200:
            #把文件名写入数据库
            photo = Photo()
            photo.photo_name = filename
            photo.user_id = current_user.id
            db.session.add(photo)
            db.session.commit()
            old_filename = os.path.join(Baseconfig.UPLOADED_PHOTOS_DEST,filename)#定义要删除的文件
            os.remove(old_filename)#删除文件
            flash('图片上传成功')
            return redirect(url_for('photo_bp.my_photo'))
        else:
            flash('图片上传失败')
            return redirect(url_for('photo_bp.photo_upload'))
    return render_template('photo/photo_upload.html',types=types)


#我的相册
@photo_bp.route('/my_photo',methods=['GET','POST'])
@login_required
def my_photo():
    types = Article_type.query.all()
    page = request.args.get('page',1,type=int)
    pagination = Photo.query.filter(Photo.user_id==current_user.id).order_by(Photo.p_datetime.desc()).paginate(page=page,per_page=5,error_out=False)
    return render_template('photo/my_photo.html',pagination=pagination,types=types)


#图片删除
@photo_bp.route('/photo_del',methods=['GET','POST'])
@login_required
def photo_del():
    pid = request.args.get('pid')
    photo = Photo.query.get(pid)
    file_del(photo.photo_name)#删除阿里云图片
    db.session.delete(photo)
    db.session.commit()
    flash('图片删除成功')
    return redirect(url_for('photo_bp.my_photo'))