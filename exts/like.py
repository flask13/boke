from flask import flash, redirect, url_for
from flask_login import current_user
from sqlalchemy import and_

from apps.models.ariticle import Like, Article, Favorite
from exts import db

#取消点赞
def del_like(aid):
    article = Article.query.filter(Article.id == aid).first()
    if not article:
        flash('该文章不存在')
        return redirect(url_for('article_bp.details'))
    like = Like.query.filter(and_(Like.article_id==aid,Like.user_id==current_user.id)).first()
    db.session.delete(like)
    article.like-=1



#点赞
def get_like(aid):
    article = Article.query.filter(Article.id == aid).first()
    if not article:
        flash('该文章不存在')
        return redirect(url_for('article_bp.details'))
    like_es = Like()
    like_es.user_id = current_user.id
    like_es.article_id = aid
    db.session.add(like_es)
    article.like+=1


#取消收藏
def del_favorite(aid):
    article = Article.query.filter(Article.id == aid).first()
    favorite = Favorite.query.filter(and_(Favorite.article_id == aid, Favorite.user_id == current_user.id)).first()
    db.session.delete(favorite)
    article.Favorites -= 1


#收藏
def get_favorite(aid):
    article = Article.query.filter(Article.id == aid).first()
    favorite = Favorite()
    favorite.user_id = current_user.id
    favorite.article_id = aid
    article.Favorites += 1
    db.session.add(favorite)
