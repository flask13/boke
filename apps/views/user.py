import os
import random
from io import BytesIO


from flask_login import login_user,current_user,logout_user,login_required
from flask import Blueprint, render_template, request, flash, make_response, redirect, url_for

from werkzeug.security import generate_password_hash, check_password_hash

from apps.models.ariticle import Article, Article_type, Message
from apps.models.user import User, Personal_information
from config import Baseconfig
from exts import db, cache, photos
from exts.captcha import generate_captcha
from exts.mail_send import send_mail

user_bp = Blueprint('user_bp',__name__,url_prefix='/user')#创建蓝图


#首页
@user_bp.route('/index')
def index():
    types = Article_type.query.all()
    page = request.args.get('page',1,type=int)
    pagination = Article.query.paginate(page=page,per_page=5,error_out=False)
    return render_template('index.html',pagination=pagination,types=types)


#把验证码返回到前端页面
@user_bp.route('/captcha_show')
def captcha_show():
    im,code = generate_captcha(4)
    cache.set('cc',code,timeout=180)#把code存入redis
    buffer = BytesIO()#创建缓存对象,以二进制的形式存储
    im.save(buffer,'JPEG')#存入缓存
    image = buffer.getvalue()#得到图片
    response = make_response(image)#自定义response
    response.headers['Content-Type'] = 'image/jpg'#设置响应头
    return response


#注册
@user_bp.route('/register',methods=['GET','POST'])
def register():
    types = Article_type.query.all()
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        repassword = request.form.get('repassword')
        email = request.form.get('email')
        phone = request.form.get('phone')
        captcha = request.form.get('captcha')
        new_captcha = cache.get('cc')
        if password != repassword:
            flash('您输入的两次密码不一致')
            return redirect(url_for('user_bp.register'))
        if captcha != new_captcha:
            flash('您输入的验证码有误')
            return redirect(url_for('user_bp.register'))
        #把数据写入数据库
        user = User()
        user.username = username
        user.password = generate_password_hash(password)
        user.email = email
        user.phone = phone
        db.session.add(user)
        db.session.commit()
        token = user.genereate_token()#生成token
        send_mail(subject='激活认证',recipients=['429859020@qq.com']
                  ,html='mail/send_mail.html',username=user.username,token=token)
        flash('邮件已发送')
        return redirect(url_for('user_bp.login'))
    return render_template('user/register.html',types=types)


#账户激活认证
@user_bp.route('/active/<token>')
def active(token):
    if User.active_token(token):
        flash('已完成账户认证')
        return redirect(url_for('user_bp.login'))
    else:
        flash('token有误')
        return redirect(url_for('user_bp.register'))



#登录
@user_bp.route('/login',methods=['GET','POST'])
def login():
    types = Article_type.query.all()
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        captcha = request.form.get('captcha')
        new_captcha = cache.get('cc')
        rember_me = request.form.get('rember_me')
        # 比对验证码是否正确
        if captcha != new_captcha:
            flash('验证码错误')
            return redirect(url_for('user_bp.register'))
        #验证用户名是否正确
        user = User.query.filter(User.username==username).first()
        if user:
            # 验证密码是否正确
            if check_password_hash(user.password,password):
                # 登录
                login_user(user, remember=rember_me)
                flash('登录成功')
                return redirect(url_for('user_bp.index'))
        flash('用户名或密码不存在')
        return redirect(url_for('user_bp.register'))
    return render_template('user/login.html',types=types)


#个人信息展示
@user_bp.route('/information_show')
@login_required
def information_show():
    types = Article_type.query.all()
    return render_template('user/information_show.html',types=types)


#修改个人信息
@user_bp.route('/information_modify',methods=['GET','POST'])
@login_required
def information_modify():
    if request.method == 'POST':
        username = request.form.get('username')
        email = request.form.get('email')
        phone = request.form.get('phone')
        user = User.query.get(current_user.id)
        user.username = username
        user.email = email
        user.phone = phone
        db.session.commit()
        flash('个人信息修改成功')
        return redirect(url_for('user_bp.index'))

    return redirect(url_for('user_bp.information_show'))


#退出
@user_bp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('user_bp.login'))



#密码修改
@user_bp.route('/password_modify',methods=['GET','POST'])
@login_required
def password_modify():
    types = Article_type.query.all()
    if request.method == 'POST':
        old_password = request.form.get('old_password')
        new_password = request.form.get('new_password')
        captcha = request.form.get('captcha')
        new_captcha = cache.get('cc')
        user = User.query.get(current_user.id)#得到一个用户对象
        if not check_password_hash(user.password,old_password):#密码比对
            flash('您输入的密码错误')
            return redirect(url_for('user_bp.password_modify'))
        elif captcha != new_captcha:
            flash('您输入的验证码错误')
            return redirect(url_for('user_bp.password_modify'))
        user.password = generate_password_hash(new_password)#把新密码写入数据库
        db.session.commit()
        flash('密码修改成功')
        return redirect(url_for('user_bp.index'))
    return render_template('user/password_modify.html',types=types)



#邮箱修改
@user_bp.route('/email_modify',methods=['GET','POST'])
@login_required
def email_modify():
    types = Article_type.query.all()
    if request.method == 'POST':
        old_email = request.form.get('old_email')
        new_email = request.form.get('new_email')
        captcha = request.form.get('captcha')
        new_captcha = cache.get('cc')
        user = User.query.filter(User.email==old_email).first()
        if not user:
            flash('用户不存在')
            return redirect(url_for('user_bp.email_modify'))
        elif captcha != new_captcha:
            flash('验证码错误')
            return redirect(url_for('user_bp.email_modify'))
        else:
            token = user.genereate_token()#生成token
            #发送激活邮件
            send_mail(subject='修改邮箱激活认证', recipients=['429859020@qq.com']
                      , html='mail/active_email.html', username=user.username,new_email=new_email,user_id=user.id, token=token)
            flash('邮件已发送')
            return redirect(url_for('user_bp.email_modify'))
    return render_template('user/email_modify.html',types=types)


#修改邮箱激活
@user_bp.route('/email_modify_active?<token>&new_email&user_id')
def email_modify_active(token):
    if User.active_token(token):
        new_email = request.args.get('new_email')
        user_id = request.args.get('user_id')
        user = User.query.get(user_id)
        if user:
            user.email = new_email
            db.session.commit()
            flash('邮箱修改成功')
            return redirect(url_for('user_bp.index'))
        else:
            flash('用户不存在')
            return redirect(url_for('user_bp.email_modify'))
    flash('token有误')
    return redirect(url_for('user_bp.email_modify'))



#头像上传及修改
@user_bp.route('/icon_modify',methods=['GET','POST'])
@login_required
def icon_modify():
    types = Article_type.query.all()
    if request.method == 'POST':
        icon = request.files.get('icon')
        suffix = '.'+icon.filename.rsplit('.',1)[1]#后缀名
        name = icon.filename.rsplit('.',1)[0] + str(random.randint(100,999))#拼接文件名
        filename = name + suffix
        photos.save(icon,name=filename)#保存本地文件
        user = User.query.get(current_user.id)#得到用户对象
        old_filename = os.path.join(Baseconfig.UPLOADED_PHOTOS_DEST,user.icon)#得到替换前的文件
        if old_filename != 'default.jpg':
            os.remove(old_filename)
        user.icon = filename#写进数据库
        db.session.commit()
        flash('头像更换成功')
        return render_template('user/icon_modify.html',types=types)
    return render_template('user/icon_modify.html',types=types)



#发布留言
@user_bp.route('/add_message',methods=['GET','POST'])
def add_message():
    if request.method == 'POST':
        name = request.form.get('name')
        email =request.form.get('email')
        phone =request.form.get('phone')
        content = request.form.get('content')
        message = Message()
        message.username = name
        message.content = content
        message.email = email
        message.phone = phone
        db.session.add(message)
        db.session.commit()
        flash('留言成功')
        return redirect(url_for('user_bp.index'))
    return render_template('message/add_message.html')


#展示留言
@user_bp.route('/show_message',methods=['GET','POST'])
@login_required
def show_message():
    types = Article_type.query.all()
    page = request.args.get('page',1,type=int)
    pagination = Message.query.order_by(Message.datetime.desc()).paginate(page=page,per_page=5,error_out=False)
    return render_template('message/del_message.html',pagination=pagination,types=types)

#删除留言
@user_bp.route('/del_message',methods=['GET','POST'])
@login_required
def del_message():
    mid = request.args.get('mid')
    message = Message.query.get(mid)
    if not message:
        flash('这条留言不存在')
        return redirect(url_for('user_bp.show_message'))
    db.session.delete(message)
    db.session.commit()
    flash('留言删除成功')
    return redirect(url_for('user_bp.show_message'))

#博主个人信息添加
@user_bp.route('/add_information',methods=['GET','POST'])
@login_required
def add_information():
    if request.method == 'POST':
        if Personal_information.query.first():
            flash('您的个人信息已添加，不能重复添加，如果您想修改可以进入修改页面')
            return redirect(url_for('user_bp.modify_information'))
        content = request.form.get('content')
        personal_information = Personal_information()
        personal_information.content = content
        db.session.add(personal_information)
        db.session.commit()
        flash('个人信息添加成功')
        return redirect(url_for('user_bp.show_information'))
    return render_template('user/personal_information_add.html')


#博主个人信息修改
@user_bp.route('/modify_information',methods=['GET','POST'])
@login_required
def modify_information():
    personal_information = Personal_information.query.first()
    if not personal_information:
        flash('找不到该个人信息')
        return redirect(url_for('user_bp.add_information'))
    if request.method == 'POST':
        content = request.form.get('content')
        personal_information.content = content
        db.session.commit()
        flash('个人信息修改成功')
        return redirect(url_for('user_bp.show_information'))
    return render_template('user/personal_information_modify.html',personal_information=personal_information)




#博主个人信息展示
@user_bp.route('/show_information',methods=['GET','POST'])
@login_required
def show_information():
    personal_information = Personal_information.query.first()
    return render_template('user/personal_information_show.html',personal_information=personal_information)

#忘记密码
@user_bp.route('/forget_password',methods=['GET','POST'])
def forget_password():
    if request.method == 'POST':
        username = request.form.get('username')
        captcha = request.form.get('captcha')
        new_captcha = cache.get('cc')
        if captcha != new_captcha:
            flash('验证码错误')
            return redirect(url_for('user_bp.forget_password'))
        elif not User.query.filter(User.username==username).first():
            flash('该用户不存在')
            return redirect(url_for('user_bp.forget_password'))
        else:
            return render_template('user/set_password.html',username=username)
    return render_template('user/forget_password.html')



#设置新密码
@user_bp.route('/set_password',methods=['GET','POST'])
def set_password():
    if request.method == 'POST':
        old_password = request.form.get('old_password')
        new_password = request.form.get('new_password')
        username = request.form.get('username')
        user = User.query.filter(User.username==username).first()
        if not check_password_hash(user.password,old_password):
            flash('您输入的旧密码错误')
            return redirect(url_for('user_bp.set_password'))
        user.password = generate_password_hash(new_password)
        db.session.commit()
        flash('修改密码成功')
        return redirect(url_for('user_bp.login'))
    return redirect(url_for('user_bp.set_password'))