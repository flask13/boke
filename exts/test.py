from flask import Blueprint, render_template
from werkzeug.security import generate_password_hash, check_password_hash

from config import Baseconfig
from exts.mail_send import send_mail
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

test_bp = Blueprint('test_bp',__name__)

@test_bp.route('/send_mail')
def send_mail_test():
    send_mail(subject='测试邮件', recipients=['429859020@qq.com'], html='mail/send_mail.html')
    return '邮件发送成功'


#加密
@test_bp.route('/generate_password/<password>')
def generate_password(password):
    return generate_password_hash(password)


#解密
@test_bp.route('/check_password/<g_password>')
def check_password(g_password):
    if check_password_hash(g_password,'123456'):
        return '密码正确'
    else:
        return '密码错误'


#生成token
@test_bp.route('/generate_token')
def generate_token():
    s = Serializer(Baseconfig.SECRET_KEY,expires_in=3600)
    token = s.dumps({'id':200})
    return token


#激活token
@test_bp.route('/active_token/<token>')
def active_token(token):
    s = Serializer(Baseconfig.SECRET_KEY)
    try:
        data = s.loads(token)
    except:
        return 'token有误'
    return str(data.get('id'))