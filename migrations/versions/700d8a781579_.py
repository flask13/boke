"""empty message

Revision ID: 700d8a781579
Revises: 
Create Date: 2021-06-28 20:10:38.269997

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '700d8a781579'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user',
    sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('username', sa.String(length=20), nullable=False),
    sa.Column('password', sa.String(length=128), nullable=False),
    sa.Column('email', sa.String(length=50), nullable=False),
    sa.Column('phone', sa.String(length=11), nullable=False),
    sa.Column('icon', sa.String(length=50), nullable=True),
    sa.Column('activation', sa.Boolean(), nullable=True),
    sa.Column('state', sa.Boolean(), nullable=True),
    sa.Column('datetime', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('user')
    # ### end Alembic commands ###
