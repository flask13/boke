# -*- coding: utf-8 -*-
import random

import oss2
import os
# 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
from oss2.models import BucketVersioningConfig

from config import Baseconfig

suffixes = ['jpg','tif','gif','png']#定义哪些格式的图片可以上传
#定义哪些格式的图片可以上传的函数
def file_suffix(filename):
    return '.' in filename and filename.rsplit('.',1)[1] in suffixes#‘.’在文件名中 and 后缀在列表中，则返回True

def file_upload(file):
    auth = oss2.Auth('LTAI5tRJJJGvPRa4ZNwoSd6Z', '7h1R4wBRc9ZiGutVQxJLb2xHBQv0qw')
    # yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
    # 填写Bucket名称。
    bucket = oss2.Bucket(auth, 'https://oss-cn-shanghai.aliyuncs.com', 'flaskblog20210703')
    # 必须以二进制的方式打开文件。
    # 填写本地文件的完整路径。如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件。
    if file and file_suffix(file.filename):  # 如果文件为真并且后缀在列表中
        ran = random.randint(1,1000)
        name = file.filename.rsplit('.')[0] + '_' + str(ran)
        suffix = '.' + file.filename.rsplit('.')[1]
        filename = name + suffix
        file.save(os.path.join(Baseconfig.UPLOADED_PHOTOS_DEST,filename))
        with open(os.path.join(Baseconfig.UPLOADED_PHOTOS_DEST,filename),mode='rb') as f:
            data = f.read()
            # 填写Object完整路径。Object完整路径中不能包含Bucket名称。
            ret = bucket.put_object('2021-7-3/'+filename,data,headers={'Content-Type': 'image/jpg'})#阿里云的上传的
            # 文件夹+文件名，待上传的内容，指定文件类型就可以在浏览器显示而不是下载图片
            url = bucket.sign_url('GET',filename,3600)#返回一个url
        return ret,filename



#删除阿里云的图片
def file_del(filename):
    auth = oss2.Auth('LTAI5tRJJJGvPRa4ZNwoSd6Z', '7h1R4wBRc9ZiGutVQxJLb2xHBQv0qw')
    # yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
    # 填写Bucket名称。
    bucket = oss2.Bucket(auth, 'https://oss-cn-shanghai.aliyuncs.com', 'flaskblog20210703')

    # 删除文件。<yourObjectName>表示删除OSS文件时需要指定包含文件后缀在内的完整路径，例如abc/efg/123.jpg。
    # 如需删除文件夹，请将<yourObjectName>设置为对应的文件夹名称。如果文件夹非空，则需要将文件夹下的所有object删除后才能删除该文件夹。
    bucket.delete_object('2021-7-3/'+filename)


